import requests
import pandas as pd
from bs4 import BeautifulSoup as bs


def get_articles_id(query):
    """
    Returns article IDs from PubMed matching query

        Parameters:
            query (str): A PubMed query

        Returns:
            art_list (list): List of articles IDs
    """
    
    url = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi'
    payload = {
        'db': 'pubmed',
        'retmode': 'json',
        'retmax': 40000,
        'sort': 'pub+date',
        'term': query,
    }

    r = requests.get(url, params=payload)
    art_list = r.json()['esearchresult']['idlist']

    return(art_list)


def make_request_fetch(id):
    """
    Make request to PubMed API, selecting articles by a list of PMIDs

    Parameters:
        id (list): list of PMIDs

    Returns:
        r (requests.models.Response): result from PubMed API http request

    """
    url2 = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi"
    payload = {
        'db': 'pubmed',
        'retmode': 'xml',
        'id': id,
    }
    r = requests.get(url2, params=payload)

    return(r)


def import_abstract_from_site(doi, css_selector):
    """
    Import abstract text from the journal website
    
    Parameters:
        doi (string): article unique digital object identifier
        css_selector (string): css selector pointing to abstract text
    
    Returns:
        (string): abstract text or None
    """
    
    r = requests.get('https://doi.org/' + doi)
    soup = bs(r.text, 'html.parser')
    abstract = soup.select_one(css_selector)
    
    if abstract is not None:
        abstract = abstract.text
        
    return(abstract)


def get_article_data(article):
    """
    Returns structured data from article XML results from PubMed API

    Parameters:
        article (xml): article XML results from a PubMed API query

    Returns:
        (dict): structured article data
    """

    # Title
    title = article.find('articletitle')
    if title:
        title = title.string

    # Abstract
    abstract_strings = article.find('abstract')
    if abstract_strings:
        abstract_strings = abstract_strings.stripped_strings
        abstract = ''
        for string in abstract_strings:
            abstract = abstract + string
    else:
        abstract = None

    # Date
    year = article.find('pubdate').find('year')
    if year:
        year = year.string
    else:
        year = article.find('pubmedpubdate').find('year')
        if year:
            year = year.string
    
    month = article.find('pubdate').find('month')
    if month:
        month = month.string
    else:
        month = article.find('pubmedpubdate').find('month')
        if year:
            month = month.string

    # Ids
    doi = article.find('articleid', idtype='doi')
    if doi:
        doi = doi.string
    else:
        doi = None

    pmid = article.find('articleid', idtype='pubmed')
    if pmid:
        pmid = pmid.string

    return {
        'title': title,
        'abstract': abstract,
        'year': year,
        'month': month,
        'doi': doi,
        'pmid': int(pmid)
    }


# Step 1: gather articles PMID matching criteria

print("Starting importation process")
print("==== Step 1: gathering articles IDs")

articles_id = get_articles_id(
    '("Critical care medicine"[Journal]) AND (1973:2019[dp])')

print(f"{len(articles_id)} articles matching selection criteria")


# Step 2: gather articles data from PubMed API (xml results)

print("==== Step 2: gathering articles data from PubMed")

n_min = 0
n_max = len(articles_id)
tranche = 100
n_tranche = round(n_max / tranche) + 1

r_final = ''

for i in range(n_min, n_tranche):
    start = tranche * (i)
    end = start + tranche - 1
    if end > n_max:
        end = n_max

    print(f"Requesting data from {start} to {end} ...")
    r = make_request_fetch(articles_id[start:end])
    r_final = r_final + r.text


# Step 3: extract articles data from XML results to a pandas dataframe

print("==== Step 3: extracting articles data from PubMed results")

xml = bs(r_final, features="lxml")
articles = xml.findAll('pubmedarticle')
articles_df = pd.DataFrame(
    {
        'title': [],
        'abstract': [],
        'year': [],
        'month': [],
        'doi': [],
        'pmid': [],
    }
)

for id_art, article in enumerate(articles):
    if (id_art) % 100 == 0:
        print(f"Article {id_art} out of {len(articles)}")

    article_data = get_article_data(article)
    articles_df = articles_df.append(article_data, ignore_index=True)


# Step 4: missing abstracts
# If abstract is missing from Pubmed then import from journal website

print("==== Step 4: extracting missing abstracts from journal website")

n_missing = articles_df['abstract'].isna().sum()
n_done = 0

print(f"Missing abstracts : {n_missing}")

for key, value in articles_df.iterrows():
        
    if articles_df.loc[key, 'abstract'] is None and articles_df.loc[key, 'doi'] is not None:
        abstract = import_abstract_from_site(articles_df.loc[key, 'doi'], '#article-abstract-content1 > p')
        articles_df.at[key, 'abstract'] = abstract
        n_done = n_done + 1
        
        if (n_done) % 100 == 0:
            print(f"Article {n_done} out of {n_missing}")


# Step 5: saving data

print("==== Step 5: saving data")

articles_df.to_csv('../data/ccm_articles.csv')

print("All data saved to /data/ccm_articles.csv")
