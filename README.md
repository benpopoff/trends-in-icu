# Trends in major intensive care medicine journals: A machine learning approach

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6760783.svg)](https://doi.org/10.5281/zenodo.6760783)

Code repository assiociated with the article:

Popoff B et al. Trends in major intensive care medicine journals: A machine learning approach. Journal of Critical Care 2022;72:154163. https://doi.org/10.1016/j.jcrc.2022.154163


