library(tidyverse)
library(RColorBrewer)
source("../r_utils/utils.R")

# -- Load and prepare data ----------------------------------------------------

data <- readRDS("data/data_tidy.rds")

data %>% summarise_na()

# Final dataset
data <- data %>% 
  filter(
    !is.na(abstract)
    & str_detect(type, "Journal Article")
    & year < 2020
  )

saveRDS(data, "data/data_final.rds")

# -- Exploratory data analysis ------------------------------------------------

# Number of published articles per month
data %>% 
  count(month) %>% 
  ggplot(aes(x = as.factor(month), y = n)) + 
  geom_col()

# Number of published articles per year
annual_data <- data %>% 
  count(year) %>% 
  mutate(decade = floor((year-1900)/10)) %>%
  group_by(decade) %>% 
  summarise(
    total = sum(n),
    min = min(n),
    max = max(n),
    mean = mean(n),
    median = median(n),
    sd = sd(n),
    q25 = quantile(n, 0.25),
    q75 = quantile(n, 0.75),
    .groups = "keep"
  ) %>%
  mutate(
    decade = case_when(
      decade == 7 ~ "1975-1979",
      decade == 8 ~ "1980-1989",
      decade == 9 ~ "1990-1999",
      decade == 10 ~ "2000-2009",
      decade == 11 ~ "2010-2019"
    ),
    decade = as_factor(decade)
  )

annual_data %>% 
  ggplot(aes(x = decade, y = total)) +
  geom_col()

annual_data %>% 
  ggplot(aes(x = decade)) +
  geom_boxplot(
    aes(ymin = min, lower = q25, middle = median, upper = q75, ymax = max),
    stat = "identity"
  )

data %>% 
  count(year) %>% 
  ggplot(aes(x = year, y = n)) + 
  geom_line() +
  geom_point()

# Number of published articles per year per journal
fig_nb_articles <- data %>% 
  mutate(
    journal = case_when(
      journal == "Eur J Intensive Care Med" ~ "EJICM",
      journal == "Lancet Respir Med" ~ "LRM",
      journal == "Intensive Care Med" ~ "ICM",
      journal == "Am J Respir Crit Care Med" ~ "AJRCCM",
      journal == "Crit Care Med" ~ "CCM",
      journal == "Crit Care" ~ "CC",
      journal == "J Neurotrauma" ~ "JN",
      journal == "Ann Intensive Care" ~ "AIC",
      TRUE ~ journal
    )
  ) %>% 
  group_by(journal) %>% 
  count(year) %>% 
  ggplot(aes(x = year, y = n, fill = journal)) + 
  geom_col() +
  labs(
    x = NULL,
    y = "Number of published articles"
  ) +
  theme_bw() +
  theme(
    legend.position = "bottom",
    legend.title = element_blank(),
    legend.text = element_text(size = 16),
    axis.title.y = element_text(size = 20),
    axis.text = element_text(size = 18)
  ) +
  scale_fill_brewer(palette = "RdBu")

ggsave(
  filename = "outputs/plots/figure3_nb_articles.eps",
  plot = fig_nb_articles,
  width = 16, height = 10, dpi = 600
)

# Number of articles per journal

data %>% 
  count(journal) %>% 
  mutate(
    freq = n/sum(n)
  )
